%% proposeKnotPositions

% a function to propose knot positions for cubic spline regressions 
% this is data dependent


function knotpos = proposeKnotPositions(wd)

% data is taken over the course of a month. Technically, we want a function
% on the scale of weeks to hours so we want about 4 to 720 knots.

for i = 1:14
    knotpos(i).val = ceil(linspace(1, 4609, ceil(1.5^(i+1))));
end 