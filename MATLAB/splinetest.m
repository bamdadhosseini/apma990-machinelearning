%a=spline(b,y(:)'/spline(b,eye(length(b)),x(:)'));

clear all
clc
close all

x = linspace(1, 2*pi, 100);
y = sin(x) + randn(1,100).*1e-1;
b = linspace(1, 2*pi, 10);
yb = sin(b);

% one line solution for splines
a=spline(b,y/spline(b,eye(length(b)),x));

figure(1)
hold on
plot(x,ppval(x, a ), '-b');
plot(b, yb, '*k');
plot(x, y, '-r');
hold off


% one line solution for natural splines
aa = csape(b,y(:).'/fnval(csape(b,eye(length(b)),'var'),x(:).'),...
'var');

figure(2)
hold on
plot(x,ppval(x, a ), '-b');
plot(b, yb, '*k');
plot(x, y, '-r');
hold off