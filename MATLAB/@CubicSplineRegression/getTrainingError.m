%% getTrainingError

% compute the training error of the fit using RSS

function getTrainingError(obj)
    
    % evaluate the model at all training points 
    obj.trainingerror = ...
        sum( (obj.train(:,2) - ppval(obj.model, obj.train(:,1))).^2);

end