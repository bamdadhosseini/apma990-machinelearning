% evaluateGPmodel

% Evaluate the fitted GP at a new set of points i.e. estimate the expected
% value

function [prediction, variance] = evaluateGPmodel(obj, input)

    
    KK = sparse( obj.kernel(obj.l, ...
        repmat(input, 1, length(obj.train)), ...
        repmat(obj.train(:,1)',length(input),1) ) );
    prediction = KK*obj.sol;
    variance = diag(KK'*KK);

end