function plotData(obj)

    % plot the input data of the cubic spline object
    figure;
    plot(obj.train(:,1), obj.train(:,2));
   

end