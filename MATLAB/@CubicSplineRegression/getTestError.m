%% getTestError

function [testerror] = getTestError(obj, test)
    
    % given a testing set compute the testing error
    testerror = ...
      sum( (test(:,2) - ppval(obj.model, test(:,1))).^2);
    
end