%% define the cubic spline regression class 

classdef CubicSplineRegression < handle
    
    properties (SetAccess = public)
        train % the data for the regression model. this is a vector. 
        DoF % the regularization parameter.
        knt % list of spline knots
        sol % solution to the spline regression problem
        model % the fitted model which is a polynomial object which gives 
              % predictions given an input
        trainingerror % training error
    end
    
    methods (Access = public)
        % class constructor 
        function obj = CubicSplineRegression...
                (data, knots)
           if size(data, 2) > 2; error('cubic spline LSQ only defined for 1D functions'); end
           
           obj.train = data;
           obj.DoF = length(knots);
           obj.knt = knots(:);
           obj.sol = [];
        end
        
        % defined in separate files
        
        %% plotting machinery
        plotData(obj)
        plotSpline(obj)
        
        %% fitting
        doRegression(obj)
        getFittedModel(obj)
        
        %% error calculation
        getTrainingError(obj)
        testerror = getTestError(obj, test)
    end
   
    
    
    
end % end of class definition