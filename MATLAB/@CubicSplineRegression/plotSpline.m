%% plotspline 

% plots the fitted spline after its been fitted 

function plotSpline(obj)

    figure;
    hold on

    plot(obj.train(:,1), obj.train(:,2), '.b', 'MarkerSize', 10);
    
    plot(obj.train(:,1), ppval(obj.train(:,1), obj.model), '-r');

    hold off
end