%% A function to solve the spline regression problem

function doSplineRegression(obj)

    obj.sol = obj.train(:,2)'/...
        fnval(csape(obj.knt,eye(length(obj.knt)),'var'),obj.train(:,1));
    obj.getFittedModel;
    
end