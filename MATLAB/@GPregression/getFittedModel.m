% getFittedModel

function obj = getFittedModel(obj)

    % this returns the vector (K + \sigma_n * I)\y which can be used 
    % to get the MAP estimate of the gaussian process for new data
    
    %max(max(obj.K))
    %min(min(obj.K))
    
    obj.sol = (obj.K + obj.noiselevel*speye(size(obj.K)))\obj.train(:,2);

end