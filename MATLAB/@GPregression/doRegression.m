%% doRegression 

function obj = doRegression(obj)

    obj.getGramMatrix;
    obj.getFittedModel;   

end