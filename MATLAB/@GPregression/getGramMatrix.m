% getGramMatrix 

% construct the Gram matrix for GP regression

function obj = getGramMatrix(obj)

    
    obj.K = sparse( obj.kernel(obj.l, ...
        repmat(obj.train(:,1), 1, length(obj.train)), ...
        repmat(obj.train(:,1)',length(obj.train),1) ) );

end