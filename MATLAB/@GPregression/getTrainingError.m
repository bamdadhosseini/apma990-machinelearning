%% getTrainingError

% compute training error for GP regression

function obj = getTrainingError(obj)

    % evaluate the model at all training points 
    obj.trainingerror = ...
        sum( (obj.train(:,2) - obj.K*obj.sol).^2);

end