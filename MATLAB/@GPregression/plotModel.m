% plot fitted data 

function plotModel(obj)

    figure;
    hold on
    plot( obj.train(:,1), obj.train(:,2), '.b', 'MarkerSize', 10 );
    plot( obj.train(:,1), obj.K*obj.sol, '-r');
    hold off
    

end