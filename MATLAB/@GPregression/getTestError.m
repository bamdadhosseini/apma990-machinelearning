%% getTestError

function [testerror] = getTestError(obj, test)
   
    % compute the covariance matrix for the test data set 
    Ktest = sparse( obj.kernel(obj.l, ...
        repmat(test(:,1), 1, length(obj.train)), ...
        repmat(obj.train(:,1)',length(test),1) ) );

    testerror = ...
      sum( (test(:,2) - Ktest*obj.sol).^2);
    
end