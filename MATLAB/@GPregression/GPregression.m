%% Gaussian process regression class 

classdef GPregression < handle
    
    properties (SetAccess = public)
        train % the data for the regression model. this is a vector. 
        noiselevel % known noise level of data
        DoF % degrees of freedom
        sol % solution to the spline regression problem
        model % the fitted model
        trainingerror % training error
        
        kernel % function handle to the kernel function
        K % gram matrix for GP
        l % characteristic length of kernel
    end
    
    methods (Access = public)
       % class constructor 
       function obj = GPregression...
               (data, pst)
           % pst must be a structure such that 
           % pst.hndl : is a function handle to the kernel function 
           % pst.ns : is value of noise level
           % pst.l  : is the characteristic length
           
            if size(data, 2) > 2; error('cubic spline LSQ only defined for 1D functions'); end
            if ~isa(pst.hndl, 'function_handle'); error('Kernel must be a function handle'); end
            
            obj.train = data;
            obj.kernel = pst.hndl;%krnl;
            obj.noiselevel = pst.ns;%noiselvl;
            obj.l = pst.l;%charlength;
            obj.sol = [];
       end
       
       % defined in separate files 
       
       %% plotting machinery
       plotModel(obj)
       
       %% fitting
       getGramMatrix(obj)
       getFittedModel(obj)
       doRegression(obj)
       
       %% error calculation
       getTrainingError(obj)
       testerror= getTestError(obj, test)
    end

end