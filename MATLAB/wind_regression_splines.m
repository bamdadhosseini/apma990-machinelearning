%% wind data regression

% a script for regression of wind data measurements at a meteorological
% post at Trail, BC, Canada. Data is measured at 10 minute intervals
% throughtout the year.

clear all 
clc
close all

% load raw data 
load('../DATA/windData.mat');

% conver velocity and angle to cartesian coordinates 
wd= polar2Cartesian(wind);
% center data
wd.x = wd.x - mean(wd.x);
wd.y = wd.y - mean(wd.y);
wd.t = wind.time;

% the data set for a month is too big (~4500 elements, so we choose a
% random subset to improve cost of CV)

load('../DATA/datasubsets');

% plot raw data and subset 
figure;
hold on
plot(wd.x, 'b');
plot(subwd.indx, subwd.x, 'ok');
plot(testwd.indx, testwd.x, '^r');
hold off
xlabel('time interval');
ylabel('w_x');
legend('raw data', 'randomly selected subset', 'test set');

figure;
hold on
plot(wd.y, 'b');
plot(subwd.indx, subwd.y, 'ok');
plot(testwd.indx, testwd.y, '^r');
hold off
xlabel('time interval');
ylabel('w_y');
legend('raw data', 'randomly selected subset', 'test set');

% propose knot positions for spline interpolations 
knotpos = proposeKnotPositions(wd);

%% regression on wd.x 
xCV = CrossValidation1D( @CubicSplineRegression, @randomDealer,...
    [subwd.indx, subwd.x], 10, knotpos );
xCV.doCV;
xCV.plotCVResults(@ (S) length(S.val));


%% regression on wd.y
yCV = CrossValidation1D( @CubicSplineRegression, @randomDealer,...
    [subwd.indx, subwd.y], 10, knotpos );
yCV.doCV;
yCV.plotCVResults(@ (S) length(S.val));

%% now fit the data using optimal knot positions 
xSplineReg = CubicSplineRegression( [subwd.indx, subwd.x], xCV.optimalpst.val);
xSplineReg.doSplineRegression;
xSplineReg.getFittedModel;
xSplineReg.getTrainingError;
xSplineReg.plotSpline;

ySplineReg = CubicSplineRegression( [subwd.indx, subwd.y], yCV.optimalpst.val);
ySplineReg.doSplineRegression;
ySplineReg.getFittedModel;
ySplineReg.getTrainingError;
ySplineReg.plotSpline;

xtsterr= xSplineReg.getTestError([testwd.indx,testwd.x])
ytsterr = ySplineReg.getTestError([testwd.indx,testwd.y])

GPpred.x = xSplineReg.evaluateGPmodel([1:length(wd.t)]');
GPpred.y = ySplineReg.evaluateGPmodel([1:length(wd.t)]');
GPpred.t = wd.t;

predwind = Cartesian2polar(GPpred);
predwind.t = GPpred.t;




