%% GPscratch

clear all
clc
close all

% random data
x = sort(rand(100,1));
y = randn(100,1);

pst.hndl = @gaussianKernel;
pst.ns = 0.1;
pst.l = 0.01;

prob = GPregression( [x,y], pst);
prob.doRegression
prob.getTrainingError

% random test 
xt = sort( rand(100,1));
yt = randn(100,1);

[testerr] = prob.getTestError([xt, yt]);

prob.plotModel