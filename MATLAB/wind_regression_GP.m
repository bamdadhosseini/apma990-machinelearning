%% wind data regression

% a script for regression of wind data measurements at a meteorological
% post at Trail, BC, Canada. Data is measured at 10 minute intervals
% throughtout the year.

clear all 
clc
close all

% load raw data 
load('../DATA/windData_lead.mat');

% conver velocity and angle to cartesian coordinates 
wd= polar2Cartesian(wind);
% center data
wd.x = wd.x - mean(wd.x);
wd.y = wd.y - mean(wd.y);
wd.t = wind.time;

 wd.x = wd.x*0.27777;
 wd.y = wd.y*0.27777;

% the data set for a month is too big (~4500 elements, so we choose a
% random subset to improve cost of CV)

load('../DATA/datasubsets_lead');
subwd.x = subwd.x*0.27777;
 subwd.y = subwd.y*0.27777;
 testwd.x = testwd.x*0.27777;
 testwd.y = testwd.y*0.27777;
% plot raw data and subset 
figure;
hold on
plot(wd.x, 'b');
plot(subwd.indx, subwd.x, 'ok');
plot(testwd.indx, testwd.x, '^r');
hold off
xlabel('time interval');
ylabel('w_x');
legend('raw data', 'randomly selected subset', 'test set');

figure;
hold on
plot(wd.y, 'b');
plot(subwd.indx, subwd.y, 'ok');
plot(testwd.indx, testwd.y, '^r');
hold off
xlabel('time interval');
ylabel('w_y');
legend('raw data', 'randomly selected subset', 'test set');


% propose values of the characteristic length for the kernel 
charlength = proposeCharLength(wd);

% specify an array of structures as input to GPs
for i=1:length(charlength)
   param(i).val.hndl = @gaussianKernel;
   %param(i).val.hndl = @polyKernel;
   param(i).val.l = charlength(i);
   param(i).val.ns = 1; % noise level accuracy of device
end

%% regression on wd.x 
xCV = CrossValidation1D( @GPregression, @randomDealer,...
    [subwd.indx, subwd.x], 10, param );
xCV.doCV;
xCV.plotCVResults(@ (S) 4609/S.val.l);
title('w_x')
drawnow

%% regression on wd.y
yCV = CrossValidation1D( @GPregression, @randomDealer,...
    [subwd.indx, subwd.y], 10, param );
yCV.doCV;
yCV.plotCVResults(@ (S) 4609/S.val.l);
title('w_y')
drawnow
%% now fit the data using optimal knot positions 
xGPReg = GPregression( [subwd.indx, subwd.x], xCV.optimalpst.val);
xGPReg.doRegression;
xGPReg.getTrainingError;
xGPReg.plotModel;

yGPReg = GPregression( [subwd.indx, subwd.y], yCV.optimalpst.val);
yGPReg.doRegression;
yGPReg.getTrainingError;
yGPReg.plotModel;

xtsterr= xGPReg.getTestError([testwd.indx,testwd.x])/1000
ytsterr = yGPReg.getTestError([testwd.indx,testwd.y])/1000

GPpred.x = xGPReg.evaluateGPmodel([1:5:length(wd.t)]');
GPpred.y = yGPReg.evaluateGPmodel([1:5:length(wd.t)]');
GPpred.t = wd.t(1:5:end,:);

predwind = Cartesian2polar(GPpred);
predwind.t = GPpred.t;

figure
plot([1:length(wind.dir)],wind.dir, 'bo', [1:5:length(wd.t)], predwind.dir, 'r', 'MarkerSize', 2)

figure
plot([1:length(wind.dir)],wind.vel*0.2777, 'bo', [1:5:length(wd.t)], predwind.vel, 'r', 'MarkerSize', 2)

dlmwrite('pred_vel_lead_fine.data', predwind.vel) % in metres per sec
dlmwrite('pred_dir_lead_fine.data', predwind.dir) % in radians



