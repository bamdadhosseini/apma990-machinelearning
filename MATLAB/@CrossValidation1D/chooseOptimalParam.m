%% chooseOptimalParam

% function for choosing the optimal cross validation parameter set 

function chooseOptimalParam(obj)

    upperbnd = min(obj.meanCVerror + obj.stdCVerror);
    indx = find(obj.meanCVerror <= upperbnd);
    obj.optimalpst = obj.pst(indx(1));

end