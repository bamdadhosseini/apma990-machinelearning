%% CV class definition

% this is a class for cross validation of 1D data 

classdef CrossValidation1D < handle 
   
    properties
       
       dset % the entire data set 
       folds % number of folds
       train % portions of data used for training  
       test % portion of data used for testing
       dealer % is a function handle provided by the user to 
              % divide data into training and test using number of folds
       regmethodhandle % handle to the  regression class which is to be 
                       % used here 
       pst % the CV parameter, this should be consistent with 
             % what is being varied in the CV. It must be an array 
             % of structures of form pst(i).val = ... which gets passed 
             % down to reg algorithm as the DoF parameter.
       testerror % matrix of test errors for CV. (i,J) element is test 
                 % error for subset i using paramter values j
       trainingerror % matrix of training errors for CV.
       
       meanCVerror % mean and
       stdCVerror  % std of CV error 
       optimalpst % optimal parameter choice out of CV
    end
    
    methods (Access = public)
       
        function obj = CrossValidation1D...
                (regClass, dealer, data, numfolds, param)
           
            if isa(dealer, 'function_handle')
               
                obj.dset =data;
                obj.folds = numfolds;
                obj.dealer = dealer;
                [obj.train, obj.test] = dealer(data, numfolds);
                
                obj.regmethodhandle = regClass;
                obj.pst = param;

            else 
                error('the method dealer must be a function handle'); 
            end
        end
        
        % externals
        doCV(obj); % perform cross validation on data
        plotCVResults(obj, arrayfunhandle) 
        chooseOptimalParam(obj)
    end
    
end