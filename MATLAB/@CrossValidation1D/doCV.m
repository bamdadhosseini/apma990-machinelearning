%% doCV

% perform cross validation on data 

function obj = doCV(obj)

    % loop over sub sets of the data (second dimension in train
    for i=1:size(obj.train,2)
        % take current subset to be the test
        subtest = reshape( obj.train(:,i,:), size(obj.train,1), 2);
        
        % take the rest to be the training set
        subtrain = obj.train;
        subtrain(:,i,:) = [];
        subtrain = reshape( subtrain, numel(subtrain)/2, 2); % and reshape 
        
        % now we're all set for regression 
        for j = 1:length(obj.pst) % loop over parameters for each sub set
            thisreg = obj.regmethodhandle(subtrain, obj.pst(j).val);  % do regression
            thisreg.doRegression;
            thisreg.getTrainingError;
            obj.testerror(i,j) = ...
                (1/length(obj.train))*(thisreg.getTestError(subtest));   % compute subtest error RSS
            obj.trainingerror(i,j) = ...
                (1/length(obj.train))*(thisreg.trainingerror); % get training error for this test
        end
    end
    
    % post process data 
    obj.meanCVerror = mean(obj.testerror, 1);
    obj.stdCVerror = std(obj.testerror, 1);
    obj.chooseOptimalParam;
end