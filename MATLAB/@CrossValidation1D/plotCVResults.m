%% plotCVResults 

% plot the results of cross validation 


function plotCVResults(obj, arrayfunhandle)

    if nargin < 2
        arrayfunhandle = @(S) S;
    end
    
    
    xvals = arrayfun( arrayfunhandle, obj.pst ); % values on the x-axis
    
    figure;
    hold on
       % ones(size(xvals)).*min(obj.meanCVerror + obj.stdCVerror) , '--k');
    errorbar(xvals, obj.meanCVerror, obj.stdCVerror, '*r');
    plot(xvals, obj.meanCVerror, '-b');
    ones(size(xvals)).*min(obj.meanCVerror + obj.stdCVerror);
    
    plot(xvals, ones(size(xvals)).*...
        min(obj.meanCVerror + obj.stdCVerror), '--k');

    xlabel('DoF');
    ylabel('CV- RSS');
    hold off
end