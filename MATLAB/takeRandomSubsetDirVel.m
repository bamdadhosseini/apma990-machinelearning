%% choose a random subset of data 

function [sub, test] = takeRandomSubsetDirVel(data, N, M)

    % generate a random permutation 
    p = randperm(length(data.vel));
    
    sub.indx = sort(p(1:N)');
    sub.vel = data.vel(sub.indx);
    sub.dir = data.dir(sub.indx);
    sub.t = data.t(sub.indx);
    
    test.indx = sort(p(N+1:N+M)');
    test.vel = data.vel(test.indx);
    test.dir = data.dir(test.indx);
    test.t = data.t(test.indx);
    
    
end