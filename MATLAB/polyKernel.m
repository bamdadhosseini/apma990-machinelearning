%% polykernel

% polynomial kernel 

function value = polyKernel(l, x1, x2)

    value = ((1/l)*(l - abs(x1 - x2))).^(3);
    value = value.*(value > 0);

end