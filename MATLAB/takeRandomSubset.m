%% choose a random subset of data 

function [sub, test] = takeRandomSubset(data, N, M)

    % generate a random permutation 
    p = randperm(length(data.x));
    
    sub.indx = sort(p(1:N)');
    sub.x = data.x(sub.indx);
    sub.y = data.y(sub.indx);
    sub.t = data.t(sub.indx);
    
    test.indx = sort(p(N+1:N+M)');
    test.x = data.x(test.indx);
    test.y = data.y(test.indx);
    test.t = data.t(test.indx);
    
    
end