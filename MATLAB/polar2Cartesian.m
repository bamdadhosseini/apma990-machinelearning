%% convert wind data to cartesian coordinates 

function [wd] = polar2Cartesian( wind )

    wd.x = wind.vel.*cos(wind.dir.*(2*pi/360));
    wd.y = wind.vel.*sin(wind.dir.*(2*pi/360));

end