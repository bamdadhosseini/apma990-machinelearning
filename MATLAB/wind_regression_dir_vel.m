% wind regression on direction and velocity

% a script for regression of wind data measurements at a meteorological
% post at Trail, BC, Canada. Data is measured at 10 minute intervals
% throughtout the year.

clear all 
clc
close all

% load raw data 
load('../DATA/windData_lead.mat');

wd = wind;
wd.vel = wd.vel % convert to metres per sec
wd.t = wind.time;

load('../DATA/datasubsets_dir_vel_lead');
subwd.vel = subwd.vel;
testwd.vel = testwd.vel;

% plot raw data and subset 
figure;
hold on
plot(wd.dir-mean(wind.dir), 'b');
plot(subwd.indx, subwd.dir, 'ok');
plot(testwd.indx, testwd.dir, '^r');
hold off
xlabel('time interval');
ylabel('w_{dir}');
legend('raw data', 'randomly selected subset', 'test set');

figure;
hold on
plot(wd.vel-mean(wind.vel), 'b');
plot(subwd.indx, subwd.vel, 'ok');
plot(testwd.indx, testwd.vel, '^r');
hold off
xlabel('time interval');
ylabel('w_{vel}');
legend('raw data', 'randomly selected subset', 'test set');

% propose values of the characteristic length for the kernel 
charlength = proposeCharLength(wd);

% specify an array of structures as input to GPs
for i=1:length(charlength)
   param(i).val.hndl = @gaussianKernel;
   %param(i).val.hndl = @polyKernel;
   param(i).val.l = charlength(i);
   param(i).val.ns = 1; % noise level accuracy of device
end

%% regression on wd.vel
xCV = CrossValidation1D( @GPregression, @randomDealer,...
    [subwd.indx, subwd.dir], 10, param );
xCV.doCV;
xCV.plotCVResults(@ (S) 4609/S.val.l);
title('w_dir')
drawnow

%% regression on wd.dir
yCV = CrossValidation1D( @GPregression, @randomDealer,...
    [subwd.indx, subwd.vel], 10, param );
yCV.doCV;
yCV.plotCVResults(@ (S) 4609/S.val.l);
title('w_vel')
drawnow

%% now fit the data using optimal knot positions 
xGPReg = GPregression( [subwd.indx, subwd.dir], xCV.optimalpst.val);
xGPReg.doRegression;
xGPReg.getTrainingError;
xGPReg.plotModel;

yGPReg = GPregression( [subwd.indx, subwd.vel], yCV.optimalpst.val);
yGPReg.doRegression;
yGPReg.getTrainingError;
yGPReg.plotModel;


xtsterr= xGPReg.getTestError([testwd.indx,testwd.vel])/1000
ytsterr = yGPReg.getTestError([testwd.indx,testwd.dir])/1000

GPpred.dir = mean(wind.dir)+ xGPReg.evaluateGPmodel([1:6:length(wd.t)]');
GPpred.vel = mean(wind.vel)+ yGPReg.evaluateGPmodel([1:6:length(wd.t)]');
GPpred.t = wd.t(1:6:end,:);

predwind = GPpred;
predwind.vel = predwind.vel*1/3.6; %convert to m/s

figure
plot([1:length(wind.dir)]*10/(60*24),wind.vel*1/3.6, 'bo', [1:6:length(wd.t)]*10/(60*24), predwind.vel, 'r', 'MarkerSize', 2,  'LineWidth', 5)
xlabel('time (days)')
ylabel('wind direction (deg)')

figure
plot([1:length(wind.dir)]*10/(60*24),wind.dir, 'bo', [1:6:length(wd.t)]*10/(60*24), predwind.dir, 'r', 'MarkerSize', 2,  'LineWidth', 5)
xlabel('time (days)')
ylabel('wind velocity (m/s)')

dlmwrite('pred_vel_lead__.data', predwind.vel) % in metres per sec
dlmwrite('pred_dir_lead__.data', predwind.dir) % in radians