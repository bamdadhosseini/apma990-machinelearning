%% convert wind data back to polar coordinates

function [wind] = Cartesian2polar( wd )

    wind.vel = sqrt( (wd.x).^2 + (wd.y).^2 );
    
    wind.dir = (wd.y > 0).*(acos((wd.x)./wind.vel) ) + ...
         (wd.y <=0).*(2*pi - acos((wd.x)./wind.vel));
     
    wind.dir = wind.dir*(360/(2*pi));
    wind.dir(find(isnan(wind.dir))) = 0;

end