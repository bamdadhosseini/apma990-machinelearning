%% randomDealer

% a function to split the training data into randomly selected 
% training, validation and test sets.

function [train, test] = randomDealer(d, k)

    if size(d,2) ~= 2
        error(' randomDealer only defined for 1D data');
    end
    
    N = length(d);
    p = randperm(N);
    
    % the first k-1 chunks are used for train and val 
    train = reshape( d(p(1:(k-1)*floor(N/k)),:), floor(N/k), k-1, 2);
    test  = d(p((k-1)*floor(N/k)+1:end),:);

end