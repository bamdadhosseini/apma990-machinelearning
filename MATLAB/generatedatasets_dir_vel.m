% generate data sets 

clear all 
clc
close all

% load raw data 
load('../DATA/windData_lead.mat');

% center data
wd = wind;
wd.dir = wd.dir - mean(wd.dir);
wd.vel = wd.vel - mean(wd.vel);

wd.t = wind.time;

% the data set for a month is too big (~4500 elements, so we choose a
% random subset to improve cost of CV)

[subwd, testwd] = takeRandomSubsetDirVel(wd, 2300, 1000);

save('../DATA/datasubsets_dir_vel_lead', 'subwd', 'testwd');