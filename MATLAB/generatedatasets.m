% generate data sets 

clear all 
clc
close all

% load raw data 
load('../DATA/windData.mat');

% conver velocity and angle to cartesian coordinates 
wd= polar2Cartesian(wind);

% center data
wd.x = wd.x - mean(wd.x);
wd.y = wd.y - mean(wd.y);

wd.t = wind.time;

% the data set for a month is too big (~4500 elements, so we choose a
% random subset to improve cost of CV)

[subwd, testwd] = takeRandomSubset(wd, 2300, 1000);

save('../DATA/datasubsets', 'subwd', 'testwd');