%% gaussian kernel for GPregression

function value = gaussianKernel(l,x1, x2)

    value = exp(- (abs(x1 - x2).^2) ./ (2*l^2));

end