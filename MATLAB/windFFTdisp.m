%% read wind data and compute fourier transform 

clear all 
clc
close all 

load('../DATA/windData.mat');

wd = polar2Cartesian( wind );

figure(1);
hold on
plot(real(fft([wd.x; wd.x(end:-1:1)])), '-b');
plot(imag(fft(wd.x)), '-.r');
hold off
title('F(x)');

figure(2); 
hold on
plot(real(fft(wd.x)), '-b');
plot(imag(fft(wd.x)), '-.r');
hold off
title('F(y)');

figure(3);
plot([wd.x; wd.x(end:-1:1)])
title('x(t)');

figure(4);
plot(wd.y)
title('y(t)');

figure(5)
plot(wind.dir.*(2*pi/360));
title('wind.dir');

figure(6)
plot(wind.vel);
title('wind.vel');