%% proposeCharLengthGP

% a function for proposing characteristic length for the kernel of GP

function cl = proposeCharLength(wd)

    for i = 1:30
        cl(i) = 4609/(i*50);
    end

end 