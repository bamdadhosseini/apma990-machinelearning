%% spline class test 
clear all
clc
close all
 
 
% random data 

t = linspace(0,1, 100);
training = [t',rand(100, 1)];
knots = linspace(0,1,20);

problem = CubicSplineRegression( training, knots);

problem.plotData;
problem.doSplineRegression;
problem.getFittedModel;
problem.getTrainingError;

test = [linspace(0, 1, 25)', rand(25,1)];
test = problem.getTestError(test);

problem.plotSpline;