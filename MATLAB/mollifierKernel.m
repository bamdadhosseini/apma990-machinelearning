% mollifier kernel

function value = mollifierKernel(l, x1, x2)

    value = (1/l).*exp(-1./(1 - (abs(x1- x2).^2)/(l^2)));
    value = value.*( abs(x1-x2) < l );

end