%% cv test script 
clear all
clc
close all

% just for testing CV class as I develop 

d = [linspace(0, 10, 200)', sin(linspace(0, 10, 200)')+ 0.1*randn(200,1)];
k = 10;

figure;
plot(d(:,1), d(:,2));

for i=1:20

    param(i).val = linspace(0,10, 2*i);
    
end


cvprob = CrossValidation1D...
    (@CubicSplineRegression,@randomDealer, d, k, param);
cvprob.doCV

funhndl = @(S) length(S.val);
cvprob.plotCVResults(funhndl);