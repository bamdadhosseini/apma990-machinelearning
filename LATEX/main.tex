\documentclass{article}

\usepackage{amsmath, amsfonts}
\usepackage{enumerate}
\usepackage{graphicx}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{listings}
\usepackage{color}

\definecolor{mygreen}{rgb}{0,0.5,0}
\definecolor{mygray}{rgb}{0.5,0.5,0.5}
\definecolor{mymauve}{rgb}{0.58,0,0.82}

\lstset{ %
  %backgroundcolor=\color{white},   % choose the background color; you must add \usepackage{color} or \usepackage{xcolor}
  basicstyle=\footnotesize,        % the size of the fonts that are used for the code
  breakatwhitespace=false,         % sets if automatic breaks should only happen at whitespace
  breaklines=true,                 % sets automatic line breaking
  captionpos=b,                    % sets the caption-position to bottom
  commentstyle=\color{mygreen},    % comment style
  deletekeywords={...},            % if you want to delete keywords from the given language
  escapeinside={\%*}{*)},          % if you want to add LaTeX within your code
  extendedchars=true,              % lets you use non-ASCII characters; for 8-bits encodings only, does not work with UTF-8
  frame=single,                    % adds a frame around the code
  keepspaces=true,                 % keeps spaces in text, useful for keeping indentation of code (possibly needs columns=flexible)
  keywordstyle=\color{blue},       % keyword style
  language=Matlab,                 % the language of the code
  %morekeywords={'empirical','quadratic'},            % if you want to add more keywords to the set
  %numbers=left,                    % where to put the line-numbers; possible values are (none, left, right)
  numbersep=5pt,                   % how far the line-numbers are from the code
 % numberstyle=\tiny\color{mygray}, % the style that is used for the line-numbers
  %rulecolor=\color{black},         % if not set, the frame-color may be changed on line-breaks within not-black text (e.g. comments (green here))
  showspaces=false,                % show spaces everywhere adding particular underscores; it overrides 'showstringspaces'
  showstringspaces=false,          % underline spaces within strings only
  showtabs=false,                  % show tabs within strings adding particular underscores
  %stepnumber=2,                    % the step between two line-numbers. If it's 1, each line will be numbered
  %stringstyle=\color{mymauve},     % string literal style
  tabsize=2,                       % sets default tabsize to 2 spaces
  title=\lstname                   % show the filename of files included with \lstinputlisting; also try caption instead of title
}

\newcommand{\Jey}{\mathcal{J}}
\newcommand{\Bee}{\mathcal{B}}
\newcommand{\Emm}{\mathcal{M}}
\newcommand{\Eff}{\mathcal{F}}
\newcommand{\Ess}{\mathcal{S}}
\newcommand{\Ell}{\mathcal{L}}
\newcommand{\Exx}{\mathcal{X}}
\newcommand{\Yay}{\mathcal{Y}}
\newcommand{\reals}{\mathbb{R}}
\newcommand{\integers}{\mathbb{N}}
\newcommand{\Salg}{$\sigma$-algebra }
\newcommand{\bfP}{{\bf P}}
\newcommand{\EXP}{\text{ {\bf E}}}
\newcommand{\VAR}{\text{ {\bf Var}}}
\newcommand{\COV}{\text{ {\bf Cov}}}
\newcommand{\IND}[1]{{\bf 1}_{#1}}
\newcommand{\absv}[1]{\left| #1 \right|}
\newcommand{\norm}[2]{\left\| #1 \right\|_{#2}}
\newcommand{\BIAS}{\text{ {\bf Bias}}} 
\newcommand{\Span}{\text{ {\bf span }}}
\newcommand{\hatmu}{\hat{\mu}}
\newcommand{\hatsig}{\hat{\mathbf{\Sigma}}}
\newcommand{\diag}{\textbf{diag}}
\newcommand{\grad}{\nabla}
\newcommand{\mb}[1]{\mathbf{#1}}
\newcommand{\NN}{\mb{N}}
\newcommand{\RR}{\mb{R}}
\newcommand{\ei}{{e_i}}
\newcommand{\Ii}{\mb{I}^i}
\newcommand{\mcN}{\mathcal{N}}

\title{Regression of wind data \\ APMA990-machine learning term project}
\author{Bamdad Hosseini}
\date{\today}

\begin{document}
\maketitle

\begin{abstract}
  In this project we consider constructing a function to a fit noisy measurements of wind speed and 
direction. The data is collected at a meteorological post at Trail, British Columbia, Canada. The 
measurements are very noisy and random and for practical reasons we are interested in fitting a
smooth function to the data. For this purpose we use two different methods i.e. a cubic spline regression 
method and a Gaussian process approach with two different kernels.
\end{abstract}

\tableofcontents

\pagebreak
\section{Introduction}
This project is motivated by another project that was done in collaboration with Teck operations at 
Trail, BC, Canada. In that project we tried to estimate the rate of fugitive emissions of 
lead particles from a large industrial site. One of the main inputs the to the model used in 
that project was the wind data i.e. measurements of the wind speed and direction at various times 
throughout the duration of the study.

\subsection{Available data}
The company provided measurements from a single meteorological 
post at 10 minute intervals for the duration of a month. As expected the data is extremely noisy and so 
it is crucial for us to do some preprocessing of the wind data before using it as input to the model. 
Also, it is often the case that some of the data points are missing in the simulations. This can be 
due to failure of equipment or regular maintenance so it is useful for us to have a way of estimating the 
missing data points.

There are some dominant patterns in the data, for example since the site is located in a valley the wind 
is mostly blowing in direction of the valley which is east and south-east or north and north-west. This can 
be seen in the windrose diagram shown in Figure \ref{fig:windrose} which shows the measurements for the period of 
September 19 to August 20, 2013. Other than this common pattern we expect that the wind should behave fairly 
locally meaning that a measurement at say right now is not likely to depend on measurements of last 
week but instead is related to the measurements a few hours before or after it. With this hypothesis in mind 
we would need a regression method that is local and it is for this reason that we choose regression splines and 
Gaussian processes with local kernels.

\begin{figure}[tp]
  \centering
 \includegraphics[width=8cm, clip = true, trim= 3cm 6cm 3cm 6cm]{./figs/windrose} 
  \caption{Windrose diagram of wind data for the period of September 19 to August 20, 2013.}
\label{fig:windrose}
\end{figure}

\subsection{The setup}
As mentioned above the data is provided at 10 minute intervals. A total of 4609 data points are 
available and at each time the wind speed $v\: [m/s]$ and direction $\theta \: [rad]$ are measured.
The data set at hand does not have any missing data points but since we have a lot of data 
we can choose a random number of the measurements as our test set. There is also a minor difficulty 
in working with the speed and angle of the wind. Since the angle is measured in radians it has a 
jump at $\theta= 0$ and $2 \pi$. To get rid of this discontinuity we choose to work with the components 
of the wind data in the West-East and North-South directions. So we define 
\begin{equation}
  \label{w-def}
  w_x(t) = v(t) \cos(\theta(t)), \qquad w_y(t) = v(t) \sin(\theta(t)),
\end{equation}
and work with these parameters from here on. So the goal here is to find proper fits to $w_x(t)$ and $w_y(t)$
which are considered to be independent functions of time $t$.

\section{Regression splines}
Our first method for fitting the data are regression splines. For a detailed treatment of these methods 
see Sections 5.2 and 5.3 of \cite{elements-of-statistical}. Take $\mb{x}:= (x_1, x_2, \cdots, x_N)^T$
 to be the $N \times 1 $ 
vector of the input variables and also take $\mb{y}:=(y_1, y_2, \cdots, y_N)^T$ to be the 
vector of corresponding outputs, so we are only considering the 1D case which is the problem of interest in 
our application. Also let  $f(z)$ be the fitting function with the following form 
\begin{equation}
  \label{splinebasisexpansion}
  f(z) = \sum_{j=1}^M \beta_{j} N_{j} (z) \quad z \in \reals,
\end{equation}
where the $\beta_{j}$ are the fitting parameters and $N_{j}$ are a basis for the function $f$ which 
is going to be a spline here, so we take $N_j$ to be the cubic spline basis:
\begin{equation}
  \label{splinebasis}
  \begin{aligned}
    N_1(z) = 1, \quad N_2 (z) = z, \quad N_{j+2} = d_j(z) - d_{M-1}(z),
  \end{aligned}
\end{equation}
where 
\begin{equation*}
 % \label{splinebasis2}
  d_j(z) = \frac{(z - \xi_j)^3_+ - (z - \xi_M)^3_+}{\xi_M - \xi_j}.
\end{equation*}
the points $\xi_j$ are the knots of the spline and this model can be seen as fitting a cubic spline 
to all data between two successive knots $\xi_j$ and $\xi_{n+1}$ while imposing continuity of 
the first and second derivatives on the knots. Then the number of these knots and their position 
is the hyperparameter in our fitting and must be chosen by cross validation or a similar method. One would 
also notice that this method is not quite local since a change in a data point still affects the entire 
fit. 

As for the implementation, we define the matrix $\mb{N}$ where $\mb{N}(i,j) := N_j(x_i)$ and the 
vector $\beta := (\beta_1, \beta_2, \cdots, \beta_M)^T$ then we have 
\begin{equation}
  \label{linearfit}
  f(\mb{x}) = \mb{N}\beta.
\end{equation}
We choose to minimize the RSS and so the fitting problem can be solved via the normal equation to 
give 
\begin{equation}
  \label{splinesol}
  \beta = (\mb{N}^T\mb{N})^{-1} \mb{N}^T \mb{y}.
\end{equation}


\section{Gaussian processes}
In this section we look at a more general set of methods called Gaussian processes. For detailed treatment 
of these methods we refer the reader to the monograph by Rasmussen and Williams \cite{rasmussen-gaussian-processes}.
A Gaussian process is defined as a collection of random variables, any finite number of which have a joint 
Gaussian distribution. If $g(\mb{z})$ is a Gaussian process we write 
\begin{equation}
  \label{gpdef}
  g(\mb{z}) \sim \mathcal{GP} (m(\mb{z}), k(\mb{z}, \mb{z}')),
\end{equation}
where $m(\mb{z})$ is the mean function and $k(\mb{z}, \mb{z}')$ is the covariance function. So then given 
the functions $m$ and $k$ and any vector $\mb{z}$ the Gaussian process is completely identified and 
defines a set of Gaussian random variables.
\subsection{Connection to kernel methods}
Suppose we have access to a data set $\mb{x}$ defined previously in Section 2. Now suppose the observations 
$\mb{y}$ are centered and given as 
\begin{equation}
  \label{noisyobs}
  \mb{y} = f(\mb{x}) + \epsilon, \qquad \epsilon \sim \mcN(0, \sigma_\epsilon^2).
\end{equation}
So we assume the noise is additive and Gaussian. Now we make the assumption that the model $f$
is a Gaussian process so that 
\begin{equation}
  \label{gpmodel}
  f(\mb{x}) \sim \mcN(0, k(\mb{x}, \mb{x})). 
\end{equation}
The covariance structure $k$ defines a Gaussian distribution over the space of functions in 1D. Using the 
noise model we can write 
\begin{equation}
  \label{gpmodelnoisy}
  \mb{y} \sim \mcN(0,  k(\mb{x}, \mb{x}) + \sigma_\epsilon^2 I).
\end{equation}
Now if we are given a new set of inputs $\mb{x_*}$ then 
\begin{equation}
  \label{gpmodeleval}
  \begin{bmatrix}
    \mb{y} \\ 
    f(\mb{x_*})
  \end{bmatrix} \sim 
  \mcN \left( 0, 
    \begin{bmatrix}
      k(\mb{x}, \mb{x}) & k(\mb{x}, \mb{x_*})\\
      k(\mb{x_*}, \mb{x}) & k(\mb{x_*}, \mb{x_*})
    \end{bmatrix}\right),
\end{equation}
and by conditioning we have that 
\begin{equation}
  \label{gpsol}
  f(\mb{x_*} | \mb{x_*}, y, \mb{x}) \sim \mcN( \bar{\mb{f_*}}, \COV(\mb{f_*})),  
\end{equation}
where 
\begin{equation}
  \label{gpsoldetails}
  \begin{aligned}
    \bar{\mb{f_*}}& = k(\mb{x_*}, \mb{x}) ( k(\mb{x}, \mb{x}) + \sigma_\epsilon^2 I)^{-1} \mb{y}, \\
    \COV(\mb{f_*}) & = k(\mb{x_*}, \mb{x_*}) - k(\mb{x_*}, \mb{x})( k(\mb{x}, \mb{x}) + \sigma_\epsilon^2 I)^{-1}.
 k(\mb{x},\mb{x_*})
  \end{aligned}
\end{equation}
Now looking at the expression for $\bar{\mb{f_*}}$ we notice that by defining $\mb{w} := ( k(\mb{x}, \mb{x}) + \sigma_\epsilon^2 I)^{-1} \mb{y}$ we would have a model of the form 
\begin{equation}
  \label{kernelmethod}
   \bar{\mb{f_*}} =  k(\mb{x_*}, \mb{x}) \mb{w}
\end{equation}
which is exactly the model that we use in kernel methods. So the mean of the Gaussian process is the 
solution to a kernel method if the kernel defines a covariance function. The Gaussian process is more 
general though, for example we can retrieve information about the covariance of the output  
$\bar{\mb{f_*}}$ or we can put a prior on the solution parameters $\mb{w}$ and use 
the Bayes rule to compute the uncertainty of the solution but since in this work we are 
more interested in fitting the data we simply use the mean and take that as our fit.

\subsection{Choice of kernels}
So far we have assumed that the kernel $k$ is known but this is not the case in practice. Generally 
we would choose the kernel depending on the problem. In order for the function $k$ to be 
an appropriate kernel it should satisfy certain conditions but for our application we are 
interested in kernels that are translation invariant and also represent local behavior. For this 
purpose our first choice is the Gaussian kernel defined as 
\begin{equation}
  \label{gaussiankernel}
  k_g(x,y) = \exp \left( - \frac{|x - y|^2}{2 \ell^2} \right),
\end{equation}
where the parameter $\ell$ would represent a characteristic length of the kernel
which, roughly speaking, shows how the output of two input variables affect each other 
depending on their distance. So small $\ell$ makes the influence very local and large 
values make it global.

One of the drawbacks of the Gaussian kernel is that it is not compactly supported so the 
matrix $k(\mb{x}, \mb{x})$ will not be sparse which is not very good for performance. 
There are many compactly supported kernels in the literature but here we consider the 
polynomial kernel defined as 
\begin{equation}
  \label{polykernel}
  k_p(x,y) = \left(1- \frac{|x-y|}{\ell} \right)_+^3,
\end{equation}
where now $2 \ell$ is the size of the support of the kernel and we take $\ell$ to 
be the characteristic length again.

\section{Results}
In this section we present the results of our regression problem for the two general methods discussed 
in Sections 2 and 3. As mentioned in the introduction, we have data points that are 
measured at 10 minute intervals for about a month so there are nearly 4600 data points 
available. This is a bit large for our regression methods specially in the 
Gaussian process approach where we must invert the covariance matrix which can be 
full. To make the problem more feasible we take half of the data i.e. 2300 randomly selected 
data points for the training set. Of the remaining data points, we take 1000 random points 
as the test set. These sets are fixed between all computations. 
Figures \ref{fig:wx-data} and \ref{fig:wy-data} show the subsets that are used for the results in this 
report.
\begin{figure}[htp]
  \centering
  \includegraphics[width=11cm, clip=true, trim=0cm 6cm 0cm 6cm]{./figs/wx-data}
  \caption{Training and test subsets of $w_x$.}
\label{fig:wx-data}
\end{figure}
\begin{figure}[htp]
  \centering
  \includegraphics[width=11cm, clip=true, trim=0cm 6cm 0cm 6cm]{./figs/wy-data}
  \caption{Training and test subsets of $w_y$.}
  \label{fig:wy-data}
\end{figure}
\subsection{Cross validation}
In both the spline regression and the Gaussian process approach we need to choose 
a hyperparameter which, in ways, represents how local our model will be. In case of 
Gaussian processes we need to choose the characteristic length and in case of the splines 
we need to find the position of the knots. In all cases we use ten fold cross validation 
over the training set.
\subsubsection{Cubic splines}
We take the knots to be uniformly spread on the time axis. We start by 
splitting the time interval into 4 subintervals and then we keep splitting the subintervals 
until overfitting increases the error so we do regression on 4, 8, 16, $\cdots$ knots. 
In a sense the length 
of the entire interval divided by the number of knots is the characteristic length of the 
spline fit since our fit at a 
certain training point is mostly affected by the data points in the same subinterval as 
that data point.  Figures \ref{fig:wx-cv-sp} and \ref{fig:wy-cv-sp} show results of our 
cross validation study for the cubic spline fits. In case of $w_x$ the optimal 
number of knots is 130 and in case of $w_y$ 195. Assuming that the whole 
data set is spread over 4600 time steps the characteristic length here for $w_x$
is about 5.8 hours and for $w_y$ is 3.9 hours. The resulting fits for these 
optimal degrees of freedom are presented in Figures \ref{fig:wx-fit-sp} 
and \ref{fig:wy-fit-sp}.
\subsubsection{Gaussian processes}
We repeat a similar computation for the Gaussian processes. First we need to select the 
characteristic lengths. Results of the cross validation for the
Gaussian kernel \eqref{gaussiankernel} are presented in Figures \ref{fig:wx-cv-gp}
and \ref{fig:wy-cv-gp} which given a characteristic length of $\ell = 30.7$ for the $w_x$
data and $27.9$ for $w_y$. These values correspond to time scales of about 5 hours 
in both cases which is quite close to the result of the spline regression. The fitted functions for this 
case are presented in Figures \ref{fig:wx-fit-gp} and
\ref{fig:wy-fit-gp}. As for the polynomial kernel \eqref{polykernel}
the results are presented in Figures
\ref{fig:wx-cv-gp-rk}-\ref{fig:wy-fit-gp-rk}. Interestingly the
optimal characteristic length in this case, for both $w_x$ and $w_y$
is $460$ time steps which is much larger compared to the previous
cases of the splines or the Gaussian kernel.

\begin{figure}[htp]
  \centering
  \includegraphics[width=10cm, clip=true, trim=0cm 6cm 0cm 6cm]{./figs/wx-cv-sp}
  \caption{Ten fold cross validation for $w_x$ using cubic splines.}
\label{fig:wx-cv-sp}
\end{figure}
\begin{figure}[htp]
  \centering
  \includegraphics[width=10cm, clip=true, trim=0cm 6cm 0cm 6cm]{./figs/wy-cv-sp}
  \caption{Ten fold cross validation for $w_y$ using cubic splines.}
  \label{fig:wy-cv-sp}
\end{figure}

\begin{figure}[htp]
  \centering
  \includegraphics[width=11cm, clip=true, trim=0cm 6cm 0cm 6cm]{./figs/wx-fit-sp}
  \caption{The fit for $w_x$ data using the optimal degrees of freedom for CV using splines.}
\label{fig:wx-fit-sp}
\end{figure}
\begin{figure}[htp]
  \centering
  \includegraphics[width=11cm, clip=true, trim=0cm 6cm 0cm 6cm]{./figs/wy-fit-sp}
  \caption{The fit for $w_y$ data using the optimal degrees of freedom for CV using splines.}
  \label{fig:wy-fit-sp}
\end{figure}

\begin{figure}[htp]
  \centering
  \includegraphics[width=11cm, clip=true, trim=0cm 5.5cm 0cm 6.5cm]{./figs/wx-cv-gp}
  \caption{Cross validation for the Gaussian process method for $w_x$ using the 
Gaussian kernel.}
\label{fig:wx-cv-gp}
\end{figure}
\begin{figure}[htp]
  \centering
  \includegraphics[width=11cm, clip=true, trim=0cm 5.5cm 0cm 6.4cm]{./figs/wy-cv-gp}
  \caption{Cross validation for the Gaussian process method for $w_y$ using the 
Gaussian kernel.}
  \label{fig:wy-cv-gp}
\end{figure}

\begin{figure}[htp]
  \centering
  \includegraphics[width=10.5cm, clip=true, trim=0cm 5.5cm 0cm 6cm]{./figs/wx-fit-gp}
  \caption{The fit for $w_x$ data using the optimal degrees of freedom for CV using the Gaussian kernel.}
\label{fig:wx-fit-gp}
\end{figure}
\begin{figure}[htp]
  \centering
  \includegraphics[width=10.5cm, clip=true, trim=0cm 5.5cm 0cm 6cm]{./figs/wy-fit-gp}
  \caption{The fit for $w_y$ data using the optimal degrees of freedom for CV using the Gaussian kernel.}
  \label{fig:wy-fit-gp}
\end{figure}

\begin{figure}[htp]
  \centering
  \includegraphics[width=11cm, clip=true, trim=0cm 5.5cm 0cm 6.5cm]{./figs/wx-cv-gp-rk}
  \caption{Cross validation for the Gaussian process method for $w_x$ using the 
polynomial kernel.}
\label{fig:wx-cv-gp-rk}
\end{figure}
\begin{figure}[htp]
  \centering
  \includegraphics[width=11cm, clip=true, trim=0cm 5.5cm 0cm 6.4cm]{./figs/wy-cv-gp-rk}
  \caption{Cross validation for the Gaussian process method for $w_y$ using the polynomial kernel.}
  \label{fig:wy-cv-gp-rk}
\end{figure}

\begin{figure}[htp]
  \centering
  \includegraphics[width=11cm, clip=true, trim=0cm 6cm 0cm 6cm]{./figs/wx-fit-gp-rk}
  \caption{The fit for $w_x$ data using the optimal degrees of freedom for CV using the polynomial kernel.}
\label{fig:wx-fit-gp-rk}
\end{figure}
\begin{figure}[htp]
  \centering
  \includegraphics[width=11cm, clip=true, trim=0cm 6cm 0cm 6cm]{./figs/wy-fit-gp-rk}
  \caption{The fit for $w_y$ data using the optimal degrees of freedom for CV using the polynomial kernel.}
  \label{fig:wy-fit-gp-rk}
\end{figure}

\subsection{Accuracy}
With the cross validation out of the way we shall now use the acquired
optimal hyperparameters to check the accuracy of our different
methods. As mentioned previously, we have randomly selected 1000 data
points as out test set and tested all methods on this set. As for our
measure of error we shall use the root mean squared error 
\begin{equation}
  \label{rms}
  RMS := \sqrt{ \frac{1}{N_{test}} \sum_{(x_i, y_i) \in test} (y_i - f(x_i))^2}.
\end{equation}
Table 1 summarizes the results of our accuracy test. We can see that
all three methods have very similar performance over the test case
with the spline regression being more accurate for $w_y$ and the 
polynomial kernel being slightly better for $w_x$.
\begin{table}[htp]\label{tab:accuracy}
  \centering
  \begin{tabular}[htp]{|c | c | c |} 
\hline
  & \multicolumn{2}{|c|}{accuracy} \\
\cline{2-3}
  method & $w_x$  &  $w_y$\\
\hline 
Spline regression & 3.39 & 2.91 \\
Gaussian kernel & 3.35 & 3.00 \\
Polynomial kernel& 3.30 & 2.93 \\
\hline
\end{tabular}
  \caption{Results of accuracy test of our three methods over the test set.}
\end{table}
\pagebreak
 \section{Conclusions}
In this project we considered regression of wind data from a
meteorological post in Trail, BC, Canada. The data has large variance
and following our intuition we decided a local method should perform
best. We applied a spline regression technique as well as a Gaussian
process approach with a Gaussian kernel as well as a polynomial kernel
both of which imply local behavior but with different regularity. We
used 
cross validation to find the optimal hyperparameters and interestingly
in case of the splines and the Gaussian kernel we found a
characteristic time scale of about 6 hours to be optimal which is very
interesting. We then tested our models on an independent test set
where we found that performance of all three methods is fairly
similar.
Given the similarity of the performance over the test set and the
smaller computational cost of the spline regression then this method
seems like a better choice as compared to the Gaussian process approach.
\pagebreak
\include{bibl}
\end{document}

