;; RefTeX parse info file
;; File: /Users/bamdadhosseini/Work/Courses/apma990-machinelearning/LATEX/main.tex
;; User: bamdadhosseini (Bamdad Hosseini)

(set reftex-docstruct-symbol '(


(xr nil "\\\\\\\\\\\\")

(index-tags)

(is-multi t)

(bibview-cache)

(master-dir . "/Users/bamdadhosseini/Work/Courses/apma990-machinelearning/LATEX/")

(label-numbers ("eq:" . 1))

(bof "/Users/bamdadhosseini/Work/Courses/apma990-machinelearning/LATEX/main.tex")

(toc "toc" "    1 Introduction" "/Users/bamdadhosseini/Work/Courses/apma990-machinelearning/LATEX/main.tex" nil 2 "1" "\\section{Introduction}" 4264)

(toc "toc" "      1.1 Available data" "/Users/bamdadhosseini/Work/Courses/apma990-machinelearning/LATEX/main.tex" nil 3 "1.1" "\\subsection{Available data}" 4711)

("fig:windrose" "f" "Windrose diagram of wind data for the priod of September 19 to August 20, 2013." "/Users/bamdadhosseini/Work/Courses/apma990-machinelearning/LATEX/main.tex" nil)

(toc "toc" "      1.2 The setup" "/Users/bamdadhosseini/Work/Courses/apma990-machinelearning/LATEX/main.tex" nil 3 "1.2" "\\subsection{The setup}" 6287)

("w-def" "e" "w_x(t) = v(t) \\cos(\\theta(t)), \\qquad w_y(t) = v(t) \\sin(\\theta(t)), " "/Users/bamdadhosseini/Work/Courses/apma990-machinelearning/LATEX/main.tex" nil)

(toc "toc" "    2 Regression splines" "/Users/bamdadhosseini/Work/Courses/apma990-machinelearning/LATEX/main.tex" nil 2 "2" "\\section{Regression splines}" 7270)

("splinebasisexpansion" "e" "f(z) = \\sum_{j=1}^M \\beta_{j} N_{j} (z) \\quad z \\in \\reals, " "/Users/bamdadhosseini/Work/Courses/apma990-machinelearning/LATEX/main.tex" nil)

("splinebasis" "e" "\\begin{aligned} N_1(z) = 1, \\quad N_2 (z) = z, \\quad N_{j+2} = d_j(z) - d_{M-1}(z), " "/Users/bamdadhosseini/Work/Courses/apma990-machinelearning/LATEX/main.tex" nil)

("splinebasis2" "s" "d_j(z) = \\frac{(z - \\xi_j)^3_+ - (z - \\xi_M)^3_+}{\\xi_M - \\xi_j}. " "/Users/bamdadhosseini/Work/Courses/apma990-machinelearning/LATEX/main.tex" t)

("linearfit" "e" "f(\\mb{x}) = \\mb{N}\\beta. " "/Users/bamdadhosseini/Work/Courses/apma990-machinelearning/LATEX/main.tex" nil)

("splinesol" "e" "\\beta = (\\mb{N}^T\\mb{N})^{-1} \\mb{N}^T \\mb{y}. " "/Users/bamdadhosseini/Work/Courses/apma990-machinelearning/LATEX/main.tex" nil)

(toc "toc" "    3 Gaussian processes" "/Users/bamdadhosseini/Work/Courses/apma990-machinelearning/LATEX/main.tex" nil 2 "3" "\\section{Gaussian processes}" 9380)

("gpdef" "e" "g(\\mb{z}) \\sim \\mathcal{GP} (m(\\mb{z}), k(\\mb{z}, \\mb{z}')), " "/Users/bamdadhosseini/Work/Courses/apma990-machinelearning/LATEX/main.tex" nil)

(toc "toc" "      3.1 Connection to kernel methods" "/Users/bamdadhosseini/Work/Courses/apma990-machinelearning/LATEX/main.tex" nil 3 "3.1" "\\subsection{Connection to kernel methods}" 10175)

("noisyobs" "e" "\\mb{y} = f(\\mb{x}) + \\epsilon, \\qquad \\epsilon \\sim \\mcN(0, \\sigma_\\epsilon^2). " "/Users/bamdadhosseini/Work/Courses/apma990-machinelearning/LATEX/main.tex" nil)

("gpmodel" "e" "f(\\mb{x}) \\sim \\mcN(0, k(\\mb{x}, \\mb{x})). " "/Users/bamdadhosseini/Work/Courses/apma990-machinelearning/LATEX/main.tex" nil)

("gpmodelnoisy" "e" "\\mb{y} \\sim \\mcN(0, k(\\mb{x}, \\mb{x}) + \\sigma_\\epsilon^2 I). " "/Users/bamdadhosseini/Work/Courses/apma990-machinelearning/LATEX/main.tex" nil)

("gpmodeleval" "e" "\\begin{bmatrix} \\mb{y} " "/Users/bamdadhosseini/Work/Courses/apma990-machinelearning/LATEX/main.tex" nil)

("gpsol" "e" "f(\\mb{x_*} | \\mb{x_*}, y, \\mb{x}) \\sim \\mcN( \\bar{\\mb{f_*}}, \\COV(\\mb{f_*})), " "/Users/bamdadhosseini/Work/Courses/apma990-machinelearning/LATEX/main.tex" nil)

("gpsoldetails" "e" "\\begin{aligned} \\bar{\\mb{f_*}}& = k(\\mb{x_*}, \\mb{x}) ( k(\\mb{x}, \\mb{x}) + \\sigma_\\epsilon^2 I)^{-1" "/Users/bamdadhosseini/Work/Courses/apma990-machinelearning/LATEX/main.tex" nil)

("kernelmethod" "e" "\\bar{\\mb{f_*}} = k(\\mb{x_*}, \\mb{x}) \\mb{w} " "/Users/bamdadhosseini/Work/Courses/apma990-machinelearning/LATEX/main.tex" nil)

(toc "toc" "      3.2 Choice of kernels" "/Users/bamdadhosseini/Work/Courses/apma990-machinelearning/LATEX/main.tex" nil 3 "3.2" "\\subsection{Choice of kernels}" 12650)

("gaussiankernel" "e" "k_g(x,y) = \\exp \\left( - \\frac{|x - y|^2}{2 \\ell^2} \\right), " "/Users/bamdadhosseini/Work/Courses/apma990-machinelearning/LATEX/main.tex" nil)

("polykernel" "e" "k_p(x,y) = \\left(1- \\frac{|x-y|}{\\ell} \\right)_+^3, " "/Users/bamdadhosseini/Work/Courses/apma990-machinelearning/LATEX/main.tex" nil)

(toc "toc" "    4 Results" "/Users/bamdadhosseini/Work/Courses/apma990-machinelearning/LATEX/main.tex" nil 2 "4" "\\section{Results}" 14042)

("fig:wx-data" "f" "Training and test subsets of $w_x$." "/Users/bamdadhosseini/Work/Courses/apma990-machinelearning/LATEX/main.tex" nil)

("fig:wy-data" "f" "Training and test subsets of $w_y$." "/Users/bamdadhosseini/Work/Courses/apma990-machinelearning/LATEX/main.tex" nil)

(toc "toc" "      4.1 Cross validation" "/Users/bamdadhosseini/Work/Courses/apma990-machinelearning/LATEX/main.tex" nil 3 "4.1" "\\subsection{Cross validation}" 15249)

(toc "toc" "        4.1.1 Cubic splines" "/Users/bamdadhosseini/Work/Courses/apma990-machinelearning/LATEX/main.tex" nil 4 "4.1.1" "\\subsubsection{Cubic splines}" 15652)

("fig:wx-cv-sp" "f" "Ten fold cross validation for $w_x$ using cubic splines." "/Users/bamdadhosseini/Work/Courses/apma990-machinelearning/LATEX/main.tex" nil)

("fig:wy-cv-sp" "f" "Ten fold cross validation for $w_y$ using cubic splines." "/Users/bamdadhosseini/Work/Courses/apma990-machinelearning/LATEX/main.tex" nil)

("fig:wx-fit-sp" "f" "The fit for $w_x$ data using the optimal degrees of freedom for CV using splines." "/Users/bamdadhosseini/Work/Courses/apma990-machinelearning/LATEX/main.tex" nil)

("fig:wy-fit-sp" "f" "The fit for $w_y$ data using the optimal degrees of freedom for CV using splines." "/Users/bamdadhosseini/Work/Courses/apma990-machinelearning/LATEX/main.tex" nil)

(toc "toc" "        4.1.2 Gaussian processes" "/Users/bamdadhosseini/Work/Courses/apma990-machinelearning/LATEX/main.tex" nil 4 "4.1.2" "\\subsubsection{Gaussian processes}" 16668)

("fig:wx-cv-gp" "f" "Cross validation for the gaussian process method for $w_x$ using the gaussian kernel." "/Users/bamdadhosseini/Work/Courses/apma990-machinelearning/LATEX/main.tex" nil)

("fig:wy-cv-gp" "f" "Cross validation for the gaussian process method for $w_y$ using the gaussian kernel." "/Users/bamdadhosseini/Work/Courses/apma990-machinelearning/LATEX/main.tex" nil)

("fig:wx-fit-gp" "f" "The fit for $w_x$ data using the optimal degrees of freedom for CV using the gaussian kernel." "/Users/bamdadhosseini/Work/Courses/apma990-machinelearning/LATEX/main.tex" nil)

("fig:wy-fit-gp" "f" "The fit for $w_y$ data using the optimal degrees of freedom for CV usingthe gaussian kernel." "/Users/bamdadhosseini/Work/Courses/apma990-machinelearning/LATEX/main.tex" nil)

("fig:wx-cv-gp-rk" "f" "Cross validation for the gaussian process method for $w_x$ using the polynomial kernel." "/Users/bamdadhosseini/Work/Courses/apma990-machinelearning/LATEX/main.tex" nil)

("fig:wy-cv-gp-rk" "f" "Cross validation for the gaussian process method for $w_y$ using the polynomial kernel." "/Users/bamdadhosseini/Work/Courses/apma990-machinelearning/LATEX/main.tex" nil)

("fig:wx-fit-gp-rk" "f" "The fit for $w_x$ data using the optimal degrees of freedom for CV using the polynomial kernel." "/Users/bamdadhosseini/Work/Courses/apma990-machinelearning/LATEX/main.tex" nil)

("fig:wy-fit-gp-rk" "f" "The fit for $w_y$ data using the optimal degrees of freedom for CV usingthe polynomial kernel." "/Users/bamdadhosseini/Work/Courses/apma990-machinelearning/LATEX/main.tex" nil)

(toc "toc" "      4.2 Accuracy" "/Users/bamdadhosseini/Work/Courses/apma990-machinelearning/LATEX/main.tex" nil 3 "4.2" "\\subsection{Accuracy}" 20629)

("eq:1" "e" nil "/Users/bamdadhosseini/Work/Courses/apma990-machinelearning/LATEX/main.tex" nil "")

(bof "/Users/bamdadhosseini/Work/Courses/apma990-machinelearning/LATEX/bibl.tex")

(bib "/Users/bamdadhosseini/Work/Courses/apma990-machinelearning/LATEX/references.bib")

(eof "/Users/bamdadhosseini/Work/Courses/apma990-machinelearning/LATEX/bibl.tex")

(eof "/Users/bamdadhosseini/Work/Courses/apma990-machinelearning/LATEX/main.tex")
))

